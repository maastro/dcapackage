@echo off

TITLE dca-gui

SET JAVA_PATH="%~dp0\..\java\bin"

for %%f in (*.war) do set JAR_FILE="%%~nf.war"
%JAVA_PATH%\java -Xmx1024m -jar %JAR_FILE% --spring.profiles.active=prod
