## Dutch Cancer Audit FHIR application

This package contains the microservices and dependencies for the Dutch Cancer Audit (DCA) FHIR application.

### Microservices

- jhipster-registry: service registry
- dcaservice: FHIR service
- dcagui: web user interface


### Installation

1.  Create the following PostgreSQL database: dcaservice

2.  Open the dcaservice yaml file (**./dcaservice/application-prod.yml**) and enter the database configuration for the dcaservice database.
    **Example:**

        spring:  
            datasource:  
                url: jdbc:postgresql://localhost:5432/dcaservice  
     		    username: dcaservice    
    		    password: dcaservice 

3.  Download installation files by executing **./bin/download.bat**.

4.  Install the microservices and set environment variables for configuration
 	-	Go to the 'bin' folder, right-click on **install.bat** and press 'Run as administrator'
	-	When prompted, choose a password for the service registry (the microservices will use this password to authenticate themselves at the service registry). 

             Choose a password for the service registry ...
             Password: *****

	-	When prompted, specify the username and password for the FHIR endpoint (to be provided by MRDM). 

             Enter username and password for the MRDM FHIR endpoint ...
             Username: ***** 
             Password: *****
             
    - When prompted, specify organization name and AGB/DICA identifiers.
            
            Enter organization details (for Organization FHIR resource) ...
            Organization name: maastro
            Organization AGB identifier: 20000990
            Organization DICA identifier: 302

5.  Start the microservices by running **./bin/start.bat**.

6.  Once all microservices are started, the user interface will automatically open in your web browser at [http://localhost:8080](http://localhost:8080). You can login using admin / admin to get started. 

    > For security reasons it is strongly advised to immediately change the admin password. This can be done in the [user-management](http://localhost:8080/#/user-management) panel.


### User management

To add new users, please follow the steps below.

1.  Go to the [registration page](http://localhost:8080/#/register) to create a new account.
	- Enter a new username and password
	- Confirm the password
	- Press 'Register'
2.  Activate the new account.
	- Login as administrator
	- Go to the [user-management](http://localhost:8080/#/user-management) panel
	- Find the account that you want to activate and press 'Edit'
	- In the 'Edit' panel, tick the 'Activated' checkbox and press 'Save'
