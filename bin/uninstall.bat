@echo off

echo Uninstalling DCA Services ...

echo. & echo Uninstalling jhipster-registry ...
"%~dp0..\registry\jhipster-registry.exe" uninstall

echo. & echo Uninstalling dca-service ...
"%~dp0..\dcaservice\dcaservice.exe" uninstall

echo. & echo Uninstalling dca-gui ...
"%~dp0..\dcagui\dcagui.exe" uninstall

echo. & echo Removing environment variables ...
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V security.jwt.secret
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V dcaregistry.admin.password
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V dcaservice.endpoint.username
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V dcaservice.endpoint.password
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V dcaservice.organization.name
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V dcaservice.organization.agb-identifier
REG delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /F /V dcaservice.organization.dica-identifier

echo.
pause
