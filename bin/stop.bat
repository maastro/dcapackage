@echo off

echo Stopping DCA Services ...

echo. & echo Stopping jhipster-registry ...
"%~dp0..\registry\jhipster-registry.exe" stop

echo. & echo Stopping dca-service ...
"%~dp0..\dcaservice\dcaservice.exe" stop

echo. & echo Stopping dca-gui ...
"%~dp0..\dcagui\dcagui.exe" stop

echo.
pause
