@echo off

echo Starting DCA Services ...

echo. & echo Starting jhipster-registry ...
"%~dp0..\registry\jhipster-registry.exe" start

echo.
CALL :waitForPort jhipster-registry , 8761

echo. & echo Starting dca-service ...
"%~dp0..\dcaservice\dcaservice.exe" start

echo. & echo Starting dca-gui ...
"%~dp0..\dcagui\dcagui.exe" start

CALL :waitForPort dca-service , 8081
CALL :waitForPort dca-gui , 8080

pause
start "dca-gui" http://localhost:8080
EXIT /B %ERRORLEVEL%

:waitForPort
echo. 
echo|set /p="Waiting for %~1 to register on port %~2 ..."
:checkPort
timeout 1 > nul
netstat -a -n -o | findstr :%2| findstr LISTENING > nul
IF ERRORLEVEL 1 (
 echo|set /p="." 
 goto checkPort
)
echo  OK
EXIT /B 0
