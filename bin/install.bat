@echo off

echo. & echo Choose a password for the service registry ...
SET /p dcaregistry.admin.password="Password: "

echo. & echo Enter username and password for the MRDM FHIR endpoint ...
SET /p dcaservice.endpoint.username="Username: "
SET /p dcaservice.endpoint.password="Password: "

echo. & echo Enter organization details (for Organization FHIR resource) ...
SET /p dcaservice.organization.name="Organization name: "
SET /p dcaservice.organization.agb-identifier="Organization AGB identifier: "
SET /p dcaservice.organization.dica-identifier="Organization DICA identifier: "

echo. & echo Setting environment variables ...
SETX -m security.jwt.secret %random%-%random%-%random%-%random%-%random%
SETX -m dcaregistry.admin.password "%dcaregistry.admin.password%"
SETX -m dcaservice.endpoint.username "%dcaservice.endpoint.username%"
SETX -m dcaservice.endpoint.password "%dcaservice.endpoint.password%"
SETX -m dcaservice.organization.name "%dcaservice.organization.name%"
SETX -m dcaservice.organization.agb-identifier "%dcaservice.organization.agb-identifier%"
SETX -m dcaservice.organization.dica-identifier "%dcaservice.organization.dica-identifier%"

echo. & echo Installing DCA Services ...

echo. & echo Installing jhipster-registry ...
"%~dp0..\registry\jhipster-registry.exe" install

echo. & echo Installing dca-service ...
"%~dp0..\dcaservice\dcaservice.exe" install

echo. & echo Installing dca-gui ...
"%~dp0..\dcagui\dcagui.exe" install

echo. & echo Installation of DCA services completed
echo.
pause
