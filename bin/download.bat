@echo off

SET REGISTRY_VERSION="3.0.1"
SET DCASERVICE_VERSION="1.2.1"
SET DCAGUI_VERSION="1.2.1"

SET CURL_EXECUTABLE="%~dp0\..\utils\curl-7.58.0\src\curl.exe"
SET ZIP_EXECUTABLE="%~dp0\..\utils\7-Zip\7z.exe"

echo Downloading installation files ...

if not exist "%~dp0\..\java" (
    echo. & echo Downloading Java JRE 8u166 ...
    %CURL_EXECUTABLE% -sLvk "https://bitbucket.org/maastro/dcapackage/downloads/jPortable64_8_Update_161.zip" > "%~dp0\..\jPortable64_8_Update_161.zip"
    %ZIP_EXECUTABLE% x "%~dp0\..\jPortable64_8_Update_161.zip" -o"%~dp0\..\"
    del "%~dp0\..\jPortable64_8_Update_161.zip"
)

SET REGISTRY_DESTINATION="%~dp0\..\registry\jhipster-registry-%REGISTRY_VERSION%.war"
if not exist %REGISTRY_DESTINATION% (
    echo. & echo Downloading jhipster-registry %REGISTRY_VERSION% ...
    %CURL_EXECUTABLE% -sLvk "https://bitbucket.org/maastro/jhipster-registry/downloads/jhipster-registry-%REGISTRY_VERSION%.war" > %REGISTRY_DESTINATION%
)

SET DCASERVICE_DESTINATION="%~dp0..\dcaservice\dcaservice-%DCASERVICE_VERSION%.war"
if not exist %DCASERVICE_DESTINATION% (
    echo. & echo Downloading dca-service %DCASERVICE_VERSION% ...
    %CURL_EXECUTABLE% -sLvk "https://bitbucket.org/maastro/dcaservice/downloads/dcaservice-%DCASERVICE_VERSION%.war" > %DCASERVICE_DESTINATION%
)

SET DCAGUI_DESTINATION="%~dp0..\dcagui\dcagui-%DCAGUI_VERSION%.war"
if not exist %DCAGUI_DESTINATION% (
    echo. & echo Downloading dca-gui %DCAGUI_VERSION% ...
    %CURL_EXECUTABLE% -sLvk "https://bitbucket.org/maastro/dcagui/downloads/dcagui-%DCAGUI_VERSION%.war" > %DCAGUI_DESTINATION%
)
