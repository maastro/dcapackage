@echo off

TITLE dca-service

SET JAVA_PATH="%~dp0\..\java\bin"

for %%f in (*.war) do set JAR_FILE="%%~nf.war"
%JAVA_PATH%\java -Xmx512m -jar %JAR_FILE% --spring.profiles.active=prod
